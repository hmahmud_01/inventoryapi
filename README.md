# README #

Api for django inventory api.

### What is this repository for? ###

Django rest framework is used for the api builds. application is currently hosted in heroku.
https://inventoryheroku.herokuapp.com/api/inventory/

### How do I get set up? ###

* Pull from the repo
* pip install requirements.txt
* run python manage.py runserver
* redirect to localhost:8000/api/inventory

### Who do I talk to? ###

* hmahmud01@gmail.com