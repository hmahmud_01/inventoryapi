from inventoryapi.viewsets import InventoryViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('inventory', InventoryViewset)