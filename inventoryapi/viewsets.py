from rest_framework import viewsets
from . import models
from . import serializers

class InventoryViewset(viewsets.ModelViewSet):
    queryset = models.Inventory.objects.all()
    serializer_class = serializers.InventorySerializer