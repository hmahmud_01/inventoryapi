# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Inventory(models.Model):

    name = models.CharField(max_length=100, blank=False)
    price = models.IntegerField()
    choices = (
        ('AVAILABLE', 'Product is Available'),
        ('SOLD', 'Product Not Avalable')
    )
    status = models.CharField(max_length=10, choices=choices, default="SOLD")


    def __str__(self):
        return self.name